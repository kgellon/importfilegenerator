﻿using ImportFileGenerator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImportFileGenerator.ViewModel
{
    public class MainViewModel : NotifyViewModel
    {
        private readonly ICommand _SimulateProgressCommand;
        public ICommand SimulateProgressCommand
        {
            get { return _SimulateProgressCommand; }
        }

        private string m_Title = "Import generator";

        public string  Title
        {
            get { return m_Title; }
            set { m_Title = value; OnPropertyChanged("Title"); }
        }


        private int _Progress = 0;
        public int ProgressIndicator
        {
            get { return _Progress; }
            set { _Progress = Math.Max(0, Math.Min(100, value)); OnPropertyChanged("ProgressIndicator"); }
        }

        private bool _SimulationEnabled = true;
        public bool SimulationEnabled
        {
            get { return _SimulationEnabled; }
            set { _SimulationEnabled = value; OnPropertyChanged("SimulationEnabled"); } 
        }

        public ObservableCollection<ProgressPanelViewModel> ListContent { get; }

        

        public MainViewModel()
        {
            _SimulateProgressCommand = new DelegateCommand(SimulateProgress);

            ListContent = new ObservableCollection<ProgressPanelViewModel>();

            ListContent.Add(new AustriaProgressViewModel());
            ListContent.Add(new PaskProgressViewModel());
        }

        private async void SimulateProgress()
        {
            if(SimulationEnabled)
            {
                SimulationEnabled = false;

                ProgressIndicator = 0;
                while(ProgressIndicator < 100)
                {
                    await Task.Delay(50);
                    ProgressIndicator++;
                }

                SimulationEnabled = true;
            }
        }
    }
}
