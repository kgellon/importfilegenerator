﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportFileGenerator.ViewModel
{
    public class PaskProgressViewModel : ProgressPanelViewModel
    {
        public PaskProgressViewModel()
        {
            m_GenerationInterval = 200;
        }

        protected override void OnGenerateCommand()
        {
            Console.WriteLine("Paks generation...");
        }
    }
}
