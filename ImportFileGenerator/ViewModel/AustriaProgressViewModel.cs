﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportFileGenerator.ViewModel
{
    public class AustriaProgressViewModel : ProgressPanelViewModel
    {
        public AustriaProgressViewModel()
        {
            m_GenerationInterval = 300;
        }

        protected override void OnGenerateCommand()
        {
            Console.WriteLine("Austria generation...");
        }
    }
}
