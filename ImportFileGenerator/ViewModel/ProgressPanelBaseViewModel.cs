﻿using ImportFileGenerator.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImportFileGenerator.ViewModel
{
    public class ProgressPanelViewModel : NotifyViewModel
    {
        private readonly ICommand m_StartCommand;
        public ICommand StartCommand
        {
            get { return m_StartCommand; }
        }

        private readonly ICommand m_GenerateCommand;

        public ICommand GenerateCommand
        {
            get { return m_GenerateCommand; }
        }

        private int m_GenerationProgress;
        public int GenerationProgress
        {
            get { return m_GenerationProgress; }
            private set { m_GenerationProgress = value; OnPropertyChanged("Progress"); }
        }


        protected int m_GenerationInterval = 100;
        public int GenerationInterval
        {
            get { return m_GenerationInterval; }
        }

        public ProgressPanelViewModel()
        {
            m_StartCommand = new DelegateCommand(OnStart);
            m_GenerateCommand = new DelegateCommand(OnGenerateCommand);
        }

        private bool _Generating { get; set; } = false;

        protected virtual void OnGenerateCommand()
        {
        }

        private async void OnStart()
        {
            await GenerateProgress();
        }

        public async virtual Task GenerateProgress()
        {
            if (_Generating) return;
            _Generating = true;

            GenerationProgress = 0;

            while(GenerationProgress < 100)
            {
                await Task.Delay(GenerationInterval);
                GenerationProgress++;
            }

            _Generating = false;
        }

    }
}
